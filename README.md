# Sécurité - Les injections SQL

## En solution, tu partageras un document détaillant :

### quelle technique d'injection SQL tu as utilisée pour obtenir la liste des utilisateurs ?

Pour outre passer la page de login j'ai utilisé ceci dans le password : 
```sql
' or 1=1--
```
Cela permet de modifier la requête SQL d'authentification comme suit :
```sql
SELECT * FROM users WHERE username = 'random' AND password = '' OR 1=1--';
```

cette condition étant toujours vrai, l'application nous laisse rentrer 


### qu'as-tu modifié dans le fichier login.php pour éviter la faille ?

Il faut remplacer ceci : 

```php
$sql = "SELECT * FROM users WHERE username = '$user' AND password = '$pass'";
```

par ceci : 
```php
 $sql = "SELECT * FROM users WHERE username = ? AND password = ?";
```

les "?" sont des marqueurs de positions pour indiquer où les valeurs réelles doivent être insérées

et ajouter le execute avec les valeurs réelles  
```php
   $stmt->execute([$user, $pass]);
```
### quelles autres bonnes pratiques faudrait-il mettre en œuvre pour éviter d'autres failles de sécurité ?

1- Il ne faut pas faire apparaître les mots de passes en clair dans la base de donnée (hachage)
2- Ne pas avoir toutes les données au même endroit et sur une période trop importante 
3- Vérifier les données qui sont rentrées sur le site côté serveur et côté client (dans les formulaires notamment), de même vérifier les données renvoyées par le serveur (avant le retour vers le client et avant l'affichage sur le site)
4- Garder un code à jour (dépendances)
5- Vérifier les logs et effectuer une surveillance sur la connexion à la BDD et sur les connexions des utilisateurs  
6- Se former à la sécurité en continu 
7- Vérifier les accès et les autorisations (tous les utisaliteurs ne doivent pas avoir accès aux mêmes choses)
8- Site en HTTPS